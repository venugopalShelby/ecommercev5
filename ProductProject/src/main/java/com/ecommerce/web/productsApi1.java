package com.ecommerce.web;
import java.io.BufferedReader;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.ecommerce.products.Products;
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse; import com.ecommerce.products.Products;
import com.ecommerce.web.dao.ProductsDao;
import com.google.gson.Gson;

public class productsApi1 extends HttpServlet {
	/**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
		
		// TODO Auto-generated method stub
		ProductsDao dao=new ProductsDao();
		List<Products> users=new ArrayList<>();
		users=dao.getProducts();
		Gson gson = new Gson();
		String userJSON;
		try {
			userJSON = gson.toJson(users);
			PrintWriter pw=response.getWriter();
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			pw.write(userJSON);
			pw.close();
		}catch(Exception e){
		     e.printStackTrace();
		}
	}
	
	/**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "http://localhost:3002/");
		// TODO Auto-generated method stub
		ProductsDao dao =new ProductsDao();
		Products prods=null;
		PrintWriter p =response.getWriter();
		String category=request.getParameter("category");
		String Pname=request.getParameter("Pname");
		int Pid =Integer.parseInt (request.getParameter("Pid"));
//        p.print(request.getParameter("prodDesc"));
		int Pprice=Integer.parseInt(request.getParameter("Pprice"));
		int Pquantity=Integer.parseInt(request.getParameter("Pquantity"));
		String Pdesc=request.getParameter("Pdesc");
		String Image_url=request.getParameter("image_url");
		prods =new Products(category,Pname,Pid,Pprice, Pquantity, Pdesc,Image_url);

		int upd=404;
		int rec=0;
		try {
			rec=dao.addInventory(prods);
		} catch (Exception e) {
		// TODO Auto-generated catch bloc
				e.printStackTrace();
				//failure status
		}
		if(rec>0)
		{
			upd=200;
		}
		else {
			upd=404;
		}
		String productJsonString=new Gson().toJson(upd);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		p.print(productJsonString);
		p.flush();
	}
	/**
     * @see HttpServlet#doPut(HttpServletRequest, HttpServletResponse)
     */
	public void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		//response.addHeader("Access-Control-Allow-Headers", "http://localhost:3002/");
		response.setHeader("Access-Control-Allow-Origin","*");
		// TODO Auto-generated method stub
		PrintWriter out = response.getWriter();
		ProductsDao dao = new ProductsDao();
		int status = 100;
//        out.print(request.getParameter("id"));
		String category = request.getParameter("category");
		int Pid = Integer.parseInt(request.getParameter("Pid"));
		String Pname = request.getParameter("Pname");
		int Pprice = Integer.parseInt(request.getParameter("Pprice"));
        int Pquantity = Integer.parseInt(request.getParameter("Pquantity"));
        String Pdesc = request.getParameter("Pdesc");
        String Image_url=request.getParameter("image_url");
		int rec = 0;
		try {
			rec = dao.updateProductUsingId(category,Pname,Pid,Pprice,Pquantity,Pdesc,Image_url);
			if (rec > 0) {
				status = 200;
			}
			String productJsonString = new Gson().toJson(status);
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			out.print(productJsonString);
			// System.out.println(productJsonString);
			out.flush();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void doDelete(HttpServletRequest request, HttpServletResponse response)throws ServletException, IOException{
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "*");
		response.addHeader("Access-Control-Allow-Headers", "*");
		ProductsDao dao = new  ProductsDao();
		PrintWriter out = response.getWriter();
		int Pid = Integer.parseInt(request.getParameter("Pid"));
		
		int status;
		int rec = 0;
		try {
			rec = dao.deleteProductUsingId(Pid);
		}catch(Exception e) {
			e.printStackTrace();
		}
		if(rec >0) {
			status=200;
		}else {
			status=404;
		}
		String productJsonString=new Gson().toJson(status);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		out.write(productJsonString);
//		System.out.println(rec);
		out.close();
	}
}