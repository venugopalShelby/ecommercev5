package com.ecommerce.web.dao;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;

import java.io.File;

public class S3FileUploader {

    public static void main(String[] args) {

        String bucketName = "ecommerce-aplication-images";
        String objectKey = "formal pant.png"; // This is the key for the object you are uploading, which will be the file name in the bucket.
        String filePath = "C:\\Users\\venu_gopal\\Downloads"; // The path to the file you want to upload.

        AmazonS3 s3Client = AmazonS3ClientBuilder.standard()
                .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration("your-endpoint", "AP_SOUTH_1"))
                .withCredentials(new DefaultAWSCredentialsProviderChain())
                .build();

        try {
            // Uploading a file to S3 bucket
            PutObjectRequest request = new PutObjectRequest(bucketName, objectKey, new File(filePath));
            s3Client.putObject(request);

            // Using TransferManager to upload the file with multiple threads (recommended for larger files)
            TransferManager transferManager = TransferManagerBuilder.standard()
                    .withS3Client(s3Client)
                    .build();
            Upload upload = transferManager.upload(bucketName, objectKey, new File(filePath));
            upload.waitForCompletion();

            System.out.println("File uploaded successfully");
        } catch (AmazonServiceException e) {
            e.printStackTrace();
        } catch (SdkClientException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
