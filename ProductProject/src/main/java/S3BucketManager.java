import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.Bucket;
import com.amazonaws.services.s3.model.CreateBucketRequest;
//import com.amazonaws.service.s3.model;
import java.util.List;

public class S3BucketManager {
    
    private AmazonS3 s3Client;
    
    public S3BucketManager() {
        this.s3Client = AmazonS3ClientBuilder.standard().build();
    }
    
    public Bucket createBucket(String bucketName) {
    	String buckName = "ecommerce-aplication-images";
        CreateBucketRequest request = new CreateBucketRequest(buckName);
        Bucket bucket = s3Client.createBucket(request);
        return bucket;
    }
    
    public List<Bucket> listBuckets() {
        List<Bucket> buckets = s3Client.listBuckets();
        return buckets;
    }
    public static void main(String[] args) {
        S3BucketManager bucketManager = new S3BucketManager();
        
        // Create a new bucket
        Bucket bucket = bucketManager.createBucket("my-bucket");
        System.out.println("Bucket created: " + bucket.getName());
        
        // List all existing buckets
        List<Bucket> buckets = bucketManager.listBuckets();
        System.out.println("Existing buckets:");
        for (Bucket b : buckets) {
            System.out.println("- " + b.getName());
        }
    }

}
