-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.37-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Dumping database structure for ecommerce
CREATE DATABASE IF NOT EXISTS `ecommerce` /*!40100 DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci */;
USE `ecommerce`;

-- Dumping structure for table ecommerce.ecommerce
CREATE TABLE IF NOT EXISTS `ecommerce` (
  `category` varchar(30) DEFAULT NULL,
  `product_id` int(6) NOT NULL,
  `product_name` varchar(30) DEFAULT NULL,
  `product_desc` varchar(30) DEFAULT NULL,
  `product_price` int(8) DEFAULT NULL,
  `product_quantity` int(5) DEFAULT NULL,
  `product_image` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Dumping data for table ecommerce.ecommerce: ~5 rows (approximately)
/*!40000 ALTER TABLE `ecommerce` DISABLE KEYS */;
INSERT INTO `ecommerce` (`category`, `product_id`, `product_name`, `product_desc`, `product_price`, `product_quantity`, `product_image`) VALUES
	('fashion', 101, 'Shirt', 'red color new brand', 1200, 2, 'https://ecommerce-aplication-images.s3.ap-south-1.amazonaws.com/shirt+image.png'),
	('fashion', 102, 'pant', 'formal pant', 1600, 4, 'https://ecommerce-aplication-images.s3.ap-south-1.amazonaws.com/formal+pant.png'),
	('Electronics', 103, 'earphones', 'brand new ', 1000, 3, 'https://ecommerce-aplication-images.s3.ap-south-1.amazonaws.com/wired+earphones.png'),
	('Electronics', 104, 'earbuds bolt', ' new brand', 1999, 1, 'https://ecommerce-aplication-images.s3.ap-south-1.amazonaws.com/wireless+earphones.png'),
	('fashion', 105, 't-shirt', 'brand shirt', 123456, 5, 'https://flipkart/fashion/tshirts');
/*!40000 ALTER TABLE `ecommerce` ENABLE KEYS */;

-- Dumping structure for table ecommerce.products
CREATE TABLE IF NOT EXISTS `products` (
  `product_id` int(6) NOT NULL,
  `product_name` varchar(30) DEFAULT NULL,
  `product_desc` varchar(30) DEFAULT NULL,
  `product_price` int(8) DEFAULT NULL,
  `product_quantity` int(5) DEFAULT NULL,
  PRIMARY KEY (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Dumping data for table ecommerce.products: ~0 rows (approximately)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Dumping structure for table ecommerce.users
CREATE TABLE IF NOT EXISTS `users` (
  `user_name` varchar(30) DEFAULT NULL,
  `password` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

-- Dumping data for table ecommerce.users: ~3 rows (approximately)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`user_name`, `password`) VALUES
	('surya', '1234'),
	('gopal', '5678'),
	('malli', '1357');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
